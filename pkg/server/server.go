package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"reflect"
	"strings"
	"time"

	middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/pkg/errors"
	"github.com/rs/cors"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/jcorry/morellis-icecream/pkg/look"
	"gitlab.com/jcorry/morellis-icecream/pkg/schema"
)

// Server represents both the gRPC server and the REST gateway.
type Server struct {
	grpcPort         int
	grpcInterceptors []grpc.UnaryServerInterceptor
	grpcServerOpts   []grpc.ServerOption
	grpcSrv          GRPCServer

	restPort int
	restMux  *runtime.ServeMux
	restSrv  HTTPServer
}

// GRPCServer is an abstraction for grpc.Server.
type GRPCServer interface {
	Serve(lis net.Listener) error
	GracefulStop()
	RegisterService(sd *grpc.ServiceDesc, ss interface{})
	GetServiceInfo() map[string]grpc.ServiceInfo
}

// HTTPServer is an abstraction for the http.Server
type HTTPServer interface {
	ListenAndServe() error
	Shutdown(ctx context.Context) error
}

// Option represents a configurable option for the server.
type Option func(*Server)

// GRPCServiceRegistrar is a callback function to register a gRPC server.
type GRPCServiceRegistrar func(srv GRPCServer) error

// RESTServiceRegistrar is a callback function to register a REST Gateway
type RESTServiceRegistrar func(ctx context.Context, mux *runtime.ServeMux, grpcAddr string, dialOpts []grpc.DialOption) error

// New creates a Server with no registered services.
func New(opts ...Option) (*Server, error) {
	// Need to include a custom marshaller for all MIME types so we can emit default values.
	// See https://github.com/grpc-ecosystem/grpc-gateway/issues/233 for more info.
	// See https://github.com/grpc-ecosystem/grpc-gateway/issues/334 for more info on using camelCase.
	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{}))

	srv := &Server{
		grpcPort:         8081,
		grpcInterceptors: []grpc.UnaryServerInterceptor{},
		grpcServerOpts:   []grpc.ServerOption{},

		restPort: 8080,
		restMux:  mux,
	}

	for _, opt := range opts {
		opt(srv)
	}

	srv.grpcSrv = grpc.NewServer(
		append(
			srv.grpcServerOpts,
			grpc.UnaryInterceptor(middleware.ChainUnaryServer(srv.grpcInterceptors...)),
		)...,
	)

	srv.restSrv = &http.Server{
		Handler:      cors.AllowAll().Handler(srv.restMux),
		Addr:         fmt.Sprintf(":%d", srv.restPort),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}

	return srv, nil
}

// NewWithExistingServers creates a new server with the given grpc.Server and http.Server.
// It is primarily used for testing, and you should consider using New instead.
func NewWithExistingServers(serviceName string, grpcSrv GRPCServer, grpcPort int, httpSrv HTTPServer, httpPort int) *Server {
	return &Server{
		grpcPort:         grpcPort,
		grpcInterceptors: nil,
		grpcServerOpts:   nil,
		grpcSrv:          grpcSrv,

		restPort: httpPort,
		restMux:  nil,
		restSrv:  httpSrv,
	}
}

// WithGRPCPort sets the port the gRPC server will listen on. Default is 8081.
func WithGRPCPort(port int) Option {
	return func(s *Server) {
		s.grpcPort = port
	}
}

// GetGRPCPort returns the gRPC port for the server.
func (s Server) GetGRPCPort() int {
	return s.grpcPort
}

// WithRestPort sets the port the REST Gateway will listen on. Default is 8080.
func WithRestPort(port int) Option {
	return func(s *Server) {
		s.restPort = port
	}
}

// GetRestPort returns the gRPC port for the server.
func (s Server) GetRestPort() int {
	return s.restPort
}

// WithGRPCInterceptors registers the given UnaryInterceptors with the server config.
// The UnaryInterceptors will be executed in the order they are passed in.
func WithGRPCInterceptors(interceptors ...grpc.UnaryServerInterceptor) Option {
	return func(s *Server) {
		s.grpcInterceptors = interceptors
	}
}

// GetGRPCInterceptors returns the gRPC interceptors used by the gRPC server.
func (s Server) GetGRPCInterceptors() []grpc.UnaryServerInterceptor {
	return s.grpcInterceptors
}

// WithGRPCServerOpts registers the given ServerOptions with the server config.
func WithGRPCServerOpts(opts ...grpc.ServerOption) Option {
	return func(s *Server) {
		s.grpcServerOpts = opts
	}
}

// GetGRPCServerOpts returns the gRPC interceptors used by the gRPC server.
func (s Server) GetGRPCServerOpts() []grpc.ServerOption {
	return s.grpcServerOpts
}

// RegisterGRPCServices registers the given gRPC services with the server.
func (s *Server) RegisterGRPCServices(grpcServiceRegistrars ...GRPCServiceRegistrar) error {
	for _, registrar := range grpcServiceRegistrars {
		if err := registrar(s.grpcSrv); err != nil {
			return err
		}
	}
	return nil
}

// RegisterRESTServices registers the given REST Gateways with the server.
func (s *Server) RegisterRESTServices(ctx context.Context, restServiceRegistrars ...RESTServiceRegistrar) error {
	grpcAddr := fmt.Sprintf("localhost:%d", s.grpcPort)
	dopts := []grpc.DialOption{grpc.WithInsecure()}

	for _, registrar := range restServiceRegistrars {
		if err := registrar(ctx, s.restMux, grpcAddr, dopts); err != nil {
			return err
		}
	}
	return nil
}

// Run starts both the gRPC server and the REST gateway.
// This method will run each server in a go routine and then block with a waitgroup.
// Since REST service is optional, we accept a bool to switch that behavior on or off
// It will accept the interrupt signal from the OS to call the Stop method.
// This method is preferred over using the individual Start methods.
func (s *Server) Run(ctx context.Context, withRest bool) error {
	ctx, sp := look.OpenSpan(ctx)
	defer sp.Close()

	sp.Infof("starting server: grpcPort=%d; restPort=%d; withRest=%t", s.grpcPort, s.restPort, withRest)

	// create a cancel function so we can listen for the done channel
	// this ctx will get overwritten below line, but the cancel
	// function here will close the new context's done channel.
	ctx, cancel := context.WithCancel(ctx)

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	defer func() {
		signal.Stop(quit)
		cancel()
	}()

	// Use an errgroup to handle gracefully stopping each server.
	var err error
	errg, ctx := errgroup.WithContext(ctx)

	errg.Go(func() error {
		if err = s.StartGRPCServer(ctx); err != nil {
			sp.Errorf(err, "grpc server encountered an error")
		}
		return err
	})

	if withRest {
		errg.Go(func() error {
			if err = s.StartRestGateway(ctx); err != nil {
				sp.Errorf(err, "rest gateway encountered an error")
			}
			return err
		})
	}

	// Run errg.Wait() in a go routine so we can use a select to listen for the quit channel
	errc := make(chan error)

	go func() {
		defer close(errc)
		errc <- errg.Wait()
	}()

	var errs []error

	// Block until the servers exit, or an interrupt is received.
	select {
	case <-quit:
		sp.Info("interrupt signal received; shutting down server")
		err = s.Stop(ctx)
		if err != nil {
			sp.Errorf(err, "error stopping server")
			errs = append(errs, err)
		}

	// errg.Wait() will call the context's cancel funcion in the event of an error
	case <-ctx.Done():
		sp.Info("shutting down server")

		// Check if any of the servers encountered an error
		for err := range errc {
			errs = append(errs, err)
		}

		err = s.Stop(ctx)

		if err != nil {
			sp.Errorf(err, "error stopping server")
			errs = append(errs, err)
		}
	}

	return flattenErrors(errs)
}

// Stop stops both the gRPC server and the REST gateway.
// Assuming that the servers were started without error, only the rest gateway can
// return an error when shutting down. The error will either be a context.DeadlineExceeded
// error, or an error from closing a listener. The gRPC server will not return an error
// when shutting down, but may hang indefinitely.
// This method is preferred over using the individual Stop methods.
func (s *Server) Stop(ctx context.Context) error {
	var errs []error

	// Stop the rest server first so it will have a chance to complete any remaining requests
	errs = append(errs, s.StopRestGateway(ctx))

	// StopGRPCServer should never return an error.
	errs = append(errs, s.StopGRPCServer(ctx))

	return flattenErrors(errs)
}

// StartGRPCServer starts the gRPC server.
// Consider using Run instead of this method.
func (s *Server) StartGRPCServer(ctx context.Context) error {
	_, sp := look.OpenSpan(ctx)
	defer sp.Close()

	if isNilInterface(s.grpcSrv) {
		return errors.New("gRPC server does not exist")
	}

	// The span should always log when it opens and closes so you
	// should get this for free
	// span.Println("starting grpc server")
	// defer s.log.Println("grpc server stopped")

	// Start listening on the specified port
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.grpcPort))
	if err != nil {
		return errors.Wrapf(err, "failed to listen on port %d", s.grpcPort)
	}
	defer func() {
		if err = lis.Close(); err != nil {
			sp.Errorf(err, "error closing net listener")
		}
	}()

	// Register reflection service on gRPC server
	if srv, ok := s.grpcSrv.(*grpc.Server); ok {
		reflection.Register(srv)
	}

	// register api meta data for auth
	srv, ok := s.grpcSrv.(*grpc.Server)
	if ok {
		err = schema.RegisterServer(srv)
		if err != nil {
			return err
		}
	}

	sp.Infof("grpc server running on port: %d", s.grpcPort)
	return s.grpcSrv.Serve(lis)
}

// StopGRPCServer stops the gRPC server.
// Consider using Stop instead of this method.
func (s *Server) StopGRPCServer(ctx context.Context) error {
	_, sp := look.OpenSpan(ctx)
	defer sp.Close()

	if isNilInterface(s.grpcSrv) {
		return errors.New("grpcSrv does not exist")
	}
	s.grpcSrv.GracefulStop()

	return nil
}

// StartRestGateway starts the REST gateway.
// Consider using Run instead of this method.
func (s *Server) StartRestGateway(ctx context.Context) error {
	_, sp := look.OpenSpan(ctx)
	defer sp.Close()

	if isNilInterface(s.restSrv) {
		return errors.New("REST server does not exist")
	}
	sp.Infof("rest gateway running on port: %d", s.restPort)
	if err := s.restSrv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return errors.Wrap(err, "failed to start rest gateway")
	}

	return nil
}

// StopRestGateway stops the REST gateway.
// Consider using Stop instead of this method.
func (s *Server) StopRestGateway(ctx context.Context) error {
	ctx, sp := look.OpenSpan(ctx)
	defer sp.Close()

	if isNilInterface(s.restSrv) {
		return errors.New("REST server does not exist")
	}

	// shutting down an http server is best done with a context that has a timeout
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := s.restSrv.Shutdown(ctx); err != nil {
		return errors.Wrap(err, "failed to shut down rest server")
	}

	return nil
}

func isNilInterface(iface interface{}) bool {
	return iface == nil || ((reflect.ValueOf(iface).Kind() == reflect.Ptr || reflect.ValueOf(iface).Kind() == reflect.Func) && reflect.ValueOf(iface).IsNil())
}

func flattenErrors(errs []error) error {
	points := []string{}
	for _, err := range errs {
		if err != nil {
			points = append(points, fmt.Sprintf("* %s", err))
		}
	}

	if len(points) == 0 {
		return nil
	}

	return errors.Errorf("%d errors occurred:\n\t%s", len(errs), strings.Join(points, "\n\t"))
}
