package server

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Predefined error values for server endpoints to return
var (
	ErrAborted            = status.Error(codes.Aborted, "Aborted")
	ErrAlreadyExists      = status.Error(codes.AlreadyExists, "Already Exists")
	ErrCanceled           = status.Error(codes.Canceled, "Canceled")
	ErrDataLoss           = status.Error(codes.DataLoss, "Data Loss")
	ErrDeadlineExceeded   = status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	ErrFailedPrecondition = status.Error(codes.FailedPrecondition, "Failed Precondition")
	ErrInternal           = status.Error(codes.Internal, "Internal Error")
	ErrInvalidArgument    = status.Error(codes.InvalidArgument, "Invalid Argument")
	ErrNotFound           = status.Error(codes.NotFound, "Not Found")
	ErrOutOfRange         = status.Error(codes.OutOfRange, "Out Of Range")
	ErrPermissionDenied   = status.Error(codes.PermissionDenied, "Permission Denied")
	ErrResourceExhausted  = status.Error(codes.ResourceExhausted, "Resource Exhausted")
	ErrUnauthenticated    = status.Error(codes.Unauthenticated, "Unauthenticated")
	ErrUnavailable        = status.Error(codes.Unavailable, "Unavailable")
	ErrUnknown            = status.Error(codes.Unknown, "Unknown")
	ErrUnimplemented      = status.Error(codes.Unimplemented, "Not Implemented")
)
