package looktest

import (
	"bytes"
	"context"
	"log"

	"gitlab.com/jcorry/morellis-icecream/pkg/look"
)

// TestableContext returns a context that has a byte buffer logger injected into it
// Useful for capturing test output
func TestableContext(level look.LogLevel, format look.LogFormat) (context.Context, *bytes.Buffer) {
	var buf bytes.Buffer
	l := &look.LeveledLogger{
		Logger: log.New(&buf, ``, 0),
		Level:  level,
		Format: format,
	}
	return look.CtxWithLogger(context.Background(), l), &buf
}

// StdLogCtx provides a context with the stdio logger
func StdLogCtx(logLevel look.LogLevel) context.Context {
	return look.CtxWithLogger(context.Background(), look.NewStdLogger(logLevel, look.Human))
}
