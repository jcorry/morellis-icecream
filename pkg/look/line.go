package look

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-stack/stack"
)

// LogLine represents a single log line
type LogLine struct {
	Level         string `json:"lev"`
	Timestamp     string `json:"tim"`
	Name          string `json:"nam"`
	CorrelationID string `json:"cid"`
	SpanID        string `json:"sid"`
	File          string `json:"fil"`
	LineNumber    string `json:"lin"`
	Message       string `json:"msg"`
}

// NewLine populates a log Line with values and returns it.
func NewLine(lev LogLevel, s *Span, msg string, c *stack.Call) *LogLine {
	return &LogLine{
		Level:         LevelToString(lev),
		Name:          s.name,
		CorrelationID: s.cID,
		SpanID:        s.sID,
		File:          fmt.Sprintf("%#s", c),
		LineNumber:    fmt.Sprintf("%d", c),
		Message:       msg,
		Timestamp:     time.Now().String(),
	}
}

// JSON encodes the log line as JSON for machine-consumption
func (l LogLine) JSON() string {
	b, err := json.Marshal(l)
	if err != nil {
		return fmt.Sprintf("error encoding line to JSON: %#v", l)
	}
	return string(b)
}

// Human encodes the log line as a line & indent format for human-consumption
func (l LogLine) Human() string {
	return fmt.Sprintf("level=%s time=%s name=%s cid=%s sid=%s file=%s line=%s msg=%s",
		l.Level, l.Timestamp, l.Name, l.CorrelationID, l.SpanID, l.File, l.LineNumber, l.Message)
}
