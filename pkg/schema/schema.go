package schema

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io/ioutil"

	"github.com/golang/protobuf/proto" //nolint:staticcheck
	"github.com/golang/protobuf/protoc-gen-go/descriptor"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/jcorry/morellis-icecream/pkg/api/v1/schema"
	apischema "gitlab.com/jcorry/morellis-icecream/pkg/api/v1/schema"
)

var (
	fileDescriptorProtos = make(map[string]*descriptor.FileDescriptorProto)
	methodOptions        = make(map[string]*apischema.MethodOptions)
)

// RegisterServer adds service info data to the service
func RegisterServer(s *grpc.Server) error {
	if nil == s {
		return status.Error(codes.InvalidArgument, "server is nil")
	}
	for id, info := range s.GetServiceInfo() {
		err := registerServiceInfo(id, info)
		if nil != err {
			return err
		}
	}
	return nil
}

func indexMethodDescriptor(
	f *descriptor.FileDescriptorProto,
	s *descriptor.ServiceDescriptorProto,
	m *descriptor.MethodDescriptorProto) error {
	if options := m.GetOptions(); options != nil {
		ext, err := proto.GetExtension(options, apischema.E_Method)
		if nil == err {
			if opts, ok := ext.(*schema.MethodOptions); ok {
				invokeName := fmt.Sprintf("/%s.%s/%s", f.GetPackage(), s.GetName(), m.GetName())
				methodOptions[invokeName] = opts
			}
		} // If we have an error here, it just means we didn't use protobuf options
	}

	return nil
}

func indexServiceDescriptor(
	f *descriptor.FileDescriptorProto,
	s *descriptor.ServiceDescriptorProto) error {
	for _, m := range s.GetMethod() {
		err := indexMethodDescriptor(f, s, m)
		if nil != err {
			return err
		}
	}

	return nil
}

func indexFileDescriptor(f *descriptor.FileDescriptorProto) error {
	for _, s := range f.GetService() {
		err := indexServiceDescriptor(f, s)
		if nil != err {
			return err
		}
	}

	return nil
}

func registerFileDescriptor(filename string) error {
	_, found := fileDescriptorProtos[filename]
	if found {
		return nil
	}

	gzippedDescriptor := proto.FileDescriptor(filename) //nolint:staticcheck //TODO: switch this to the undeprecated version
	if len(gzippedDescriptor) == 0 {
		return status.Errorf(codes.Unavailable, "FileDescriptor for %s not found", filename)
	}

	r, err := gzip.NewReader(bytes.NewReader(gzippedDescriptor))
	if nil != err {
		return errors.Wrapf(err, "failed to build gzip reader for %s", filename)
	}
	unzipped, err := ioutil.ReadAll(r)
	if nil != err {
		return errors.Wrapf(err, "failed to unzip %s", filename)
	}

	var d descriptor.FileDescriptorProto
	err = proto.Unmarshal(unzipped, &d)
	if nil != err {
		return errors.Wrapf(err, "failed to unmarshal unzipped %s into a proto file descriptor", filename)
	}

	fileDescriptorProtos[filename] = &d

	return indexFileDescriptor(&d)
}

func registerServiceInfo(id string, i grpc.ServiceInfo) error {
	source, ok := i.Metadata.(string)
	if !ok {
		return status.Errorf(codes.Unavailable, "The service %s does not have proper metadata", id)
	}
	return registerFileDescriptor(source)
}

// MethodOptions returns the Saturn schema options for the method name given.
// The method name is the name defined by the Invoke in the generated code such as:
func MethodOptions(method string) *apischema.MethodOptions {
	return methodOptions[method]
}

// MethodAuthorizationRequired returns true if a method requires authorization based on its name
// The default value returned is true unless it can find whitelisting for it.
// The method name is the name defined by the Invoke in the generated code such as:
func MethodAuthorizationRequired(method string) bool {
	// Try to get the options but if we don't find them assume authorization is required
	options := MethodOptions(method)
	if nil == options {
		return true
	}

	// This is safe to inline since it handles null values
	return !options.GetAuthorization().GetOptional()
}
