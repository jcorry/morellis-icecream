package main

import (
	"context"
	"log"

	"github.com/jessevdk/go-flags"
	"github.com/pkg/errors"
	flavorService "gitlab.com/jcorry/morellis-icecream/flavor/service"
	identityService "gitlab.com/jcorry/morellis-icecream/identity/service"
	"gitlab.com/jcorry/morellis-icecream/pkg/look"
	grpcServer "gitlab.com/jcorry/morellis-icecream/pkg/server"
)

type options struct {
	Environment string `long:"environment" env:"ENVIRONMENT" description:"The environment the app is running in"`
	LogLevel    int    `long:"log-level" env:"LOG_LEVEL" description:"The type of log messages to capture."`
	GRPCPort    int    `long:"grpc-port" env:"GRPC_PORT" description:"The port the gRPC service will run on"`
	RESTPort    int    `long:"rest-port" env:"REST_PORT" description:"The port the REST gateway will run on"`
}

func main() {
	log.Println("starting server...")

	// Parse startup flags
	var opts options
	_, err := flags.Parse(&opts)
	if err != nil {
		log.Fatalf("FATAL ERROR: failed to parse startup options: %+v", err)
	}

	// Set up logger
	logger := look.NewStdLogger(look.LogLevel(opts.LogLevel), look.JSON) // default "0" is ERROR (i.e. nothing but errors)

	// Set main context to background
	ctx := look.CtxWithLogger(context.Background(), logger)

	// Run (blocking)
	if err := run(ctx, logger, opts); err != nil {
		log.Fatalf("FATAL ERROR: %+v", err)
	}
}

func run(ctx context.Context, logger *look.LeveledLogger, opts options) error {
	ctx = look.CtxWithLogger(ctx, logger)
	ctx, sp := look.OpenSpan(ctx)
	defer sp.Close()

	sp.Infof("app starting in environment: %v", opts.Environment)

	flavorServiceAPI := flavorService.NewServerWithEndpoints(logger)
	identityServiceAPI := identityService.NewServerWithEndpoints()

	var grpcServerOpts []grpcServer.Option
	grpcServerOpts = append(grpcServerOpts, grpcServer.WithGRPCPort(opts.GRPCPort), grpcServer.WithGRPCInterceptors(look.LogInterceptor))
	if opts.RESTPort != 0 {
		grpcServerOpts = append(grpcServerOpts, grpcServer.WithRestPort(opts.RESTPort))
	}

	s, err := grpcServer.New(
		grpcServerOpts...,
	)
	if err != nil {
		return errors.Errorf("unable to create new gRPC server: %v", err)
	}

	err = s.RegisterGRPCServices(
		flavorServiceAPI.RegisterGRPCServer,
		identityServiceAPI.RegisterGRPCServer,
	)

	if err != nil {
		return errors.Errorf("unable to register services on gRPC server: %v", err)
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	if opts.RESTPort != 0 {
		err = s.RegisterRESTServices(
			ctx,
			flavorServiceAPI.RegisterRESTGateway,
			identityServiceAPI.RegisterRESTGateway,
		)
		if err != nil {
			return err
		}
	}

	if err = s.Run(ctx, true); err != nil {
		return errors.Errorf("unable to start server: %v", err)
	}

	return nil
}
