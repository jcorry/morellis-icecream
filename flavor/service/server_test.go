package service_test

import (
	"context"
	"log"
	"net"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/jcorry/morellis-icecream/flavor/service"
	"gitlab.com/jcorry/morellis-icecream/pkg/api/v1/services/common"
	"gitlab.com/jcorry/morellis-icecream/pkg/api/v1/services/flavor"
	"gitlab.com/jcorry/morellis-icecream/pkg/look"
	"gitlab.com/jcorry/morellis-icecream/pkg/schema"
	"gitlab.com/jcorry/morellis-icecream/pkg/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

var (
	listen *bufconn.Listener
)

func TestServer_TestStatus(t *testing.T) {
	startTestServer(t)
	c := getClient(t)

	ctx := context.TODO()

	t.Run("get status", func(t *testing.T) {
		_, err := c.Status(ctx, &common.StatusRequest{})
		require.NoError(t, err)
	})
}

func TestServer_FlavorCreate(t *testing.T) {
	startTestServer(t)
	c := getClient(t)

	ctx := context.TODO()

	t.Run("create flavor", func(t *testing.T) {
		res, err := c.Create(ctx, &flavor.CreateRequest{
			Flavor: &flavor.Flavor{
				Name:        "Test",
				Description: "Test Flavor",
			},
		})
		require.EqualError(t, err, server.ErrNotFound.Error())
		require.Nil(t, res)
	})
}

func startTestServer(t *testing.T) {
	// We need a regCodeStore with a live DB
	var err error

	// bufconn provides a net.Conn implemented by a buffer
	// https://godoc.org/google.golang.org/grpc/test/bufconn
	listen = bufconn.Listen(1024 * 1024)
	s := grpc.NewServer(interceptors())

	// Set up logger
	logger := look.NewStdLogger(look.LogLevel(look.Debug), look.JSON) // default "0" is ERROR (i.e. nothing but errors)

	server := service.NewServerWithEndpoints(logger)

	err = server.RegisterGRPCServer(s)
	require.NoError(t, err)

	err = schema.RegisterServer(s)
	require.NoError(t, err)

	// This is in a go routine because it's a blocking call
	go func() {
		if err := s.Serve(listen); err != nil {
			log.Fatalf("server exited with error: %v", err)
		}
	}()

	t.Cleanup(func() {
		s.GracefulStop()
	})
}

func bufDialer(_ context.Context, _ string) (net.Conn, error) {
	return listen.Dial()
}

// Get an actual gRPC client
func getClient(t *testing.T) flavor.FlavorServiceClient {
	ctx := context.TODO()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithInsecure())
	require.NoError(t, err)

	t.Cleanup(func() {
		conn.Close()
	})

	return flavor.NewFlavorServiceClient(conn)
}

func interceptors() grpc.ServerOption {
	return grpc.ChainUnaryInterceptor(
		look.LogInterceptor,
	)
}
