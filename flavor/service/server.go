package service

import (
	"context"
	"errors"
	"fmt"

	types "github.com/golang/protobuf/ptypes/struct"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.com/jcorry/morellis-icecream/pkg/api/v1/services/common"
	"gitlab.com/jcorry/morellis-icecream/pkg/api/v1/services/flavor"
	"gitlab.com/jcorry/morellis-icecream/pkg/look"
	grpcServer "gitlab.com/jcorry/morellis-icecream/pkg/server"
	"google.golang.org/grpc"
)

type Server struct {
	*flavor.UnimplementedFlavorServiceServer
	logger *look.LeveledLogger
}

func NewServerWithEndpoints(l *look.LeveledLogger) Server {
	return Server{
		UnimplementedFlavorServiceServer: &flavor.UnimplementedFlavorServiceServer{},
		logger:                           l,
	}
}

// RegisterGRPCServer binds the Server to the generated gRPC server.
func (s *Server) RegisterGRPCServer(g grpcServer.GRPCServer) error {
	if g, ok := g.(*grpc.Server); ok {
		flavor.RegisterFlavorServiceServer(g, s)
		return nil
	}
	return errors.New("g must be of type *grpc.Server")
}

// RegisterRESTGateway binds the Server to the generated REST Gateway.
func (s *Server) RegisterRESTGateway(ctx context.Context, mux *runtime.ServeMux, grpcAddr string, dialOpts []grpc.DialOption) error {
	return flavor.RegisterFlavorServiceHandlerFromEndpoint(ctx, mux, grpcAddr, dialOpts)
}

func (s *Server) Status(ctx context.Context, req *common.StatusRequest) (*common.StatusResponse, error) {
	fmt.Println("flavor service status endpoint hit...")
	return &common.StatusResponse{
		Info: &types.Struct{},
	}, nil
}
