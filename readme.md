# Morellis Ice Cream

Morellis is my favorite ice cream store here in Atlanta. I want to build a flavor management and notification service for them.

The system will:
- allow store staff to update which flavors are active at each of their locations
- allow customers to save their favorite flavors
- notify customers of their favorite flavors being served at their preferred locations

# Architecture

The service interface is defined in .proto files in `/proto`. These are used to generate a gRPC server with REST Gateway.
Each bounded context is represented in its own .proto file and the corresponding implementation defined in a package
at the project root.

All of the server implementations are served through a common server, the bootstrapping of which is in `cmd/main.go`

Each bounded context has a `domain`, `application` (use cases) and `infrastructure` package.

# Code Generation

`make install` installs the necessary tooling on the host machine

`make generate` generates the API code from the .proto files. This target uses the 
buf (https://buf.build) to compile the proto files with the REST Gateway.

## Swagger

This also generates swagger documentation files. I don't believe these are very useful though, the 
.proto files themselves are the API documentation. If they are to be used, finding a solution that combines them
into a single UI would be good. I tried this: https://github.com/maxdome/swagger-combine but wasn't able to 
get it all working in the 20 min timebox I allowed.

# Running the service

`make run` exports necessary env vars and runs the gRPC and REST server
