SOURCES :=$(shell find ./proto -type f -name "*.proto")

.PHONY: vendor
vendor:
	go mod tidy && \
	go mod vendor

.PHONY: generate
generate:
	buf generate

.PHONY: lint
lint:
	buf lint

BUF_VERSION:=0.43.2

.PHONY: install
install:
	go install \
		google.golang.org/protobuf/cmd/protoc-gen-go \
		google.golang.org/grpc/cmd/protoc-gen-go-grpc \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2
	curl -sSL \
    	"https://github.com/bufbuild/buf/releases/download/v${BUF_VERSION}/buf-$(shell uname -s)-$(shell uname -m)" \
    	-o "$(shell go env GOPATH)/bin/buf" && \
  	chmod +x "$(shell go env GOPATH)/bin/buf"

.PHONY: run
run:
	export GRPC_PORT=9090 && \
	export REST_PORT=8080 && \
	export ENV=local && \
	export LOG_LEVEL=2 && \
	go run cmd/main.go
